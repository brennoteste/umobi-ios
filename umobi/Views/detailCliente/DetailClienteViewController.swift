//
//  DetailClienteViewController.swift
//  umoby-test
//
//  Created by Brenno de Moura on 14/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import UIKit

class DetailClienteViewController: UIViewController {
    public var id: Int = 0

    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var empresaLabel: UILabel!
    @IBOutlet weak var telefoneLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        ClienteData.request(id: self.id) {
            self.reloadData()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func reloadData() {
        nomeLabel.text = ClienteData.shared.nome
        emailLabel.text = ClienteData.shared.email
        empresaLabel.text = ClienteData.shared.empresa
        telefoneLabel.text = ClienteData.shared.telefone
    }
    
    deinit {
        ClienteData.shared.eraseData()
    }
}
