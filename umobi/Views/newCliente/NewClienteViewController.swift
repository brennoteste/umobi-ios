//
//  DetailClienteViewController.swift
//  umoby-test
//
//  Created by Brenno de Moura on 14/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import UIKit

class NewClienteViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nomeField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var empresaField: UITextField!
    @IBOutlet weak var telefoneField: UITextField!

    @IBOutlet weak var detailView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        nomeField.delegate = self
        emailField.delegate = self
        nomeField.delegate = self
        empresaField.delegate = self
        
        detailView.isHidden = true
        
        ClienteData.shared.eraseData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Pronto", style: .plain, target: self, action: #selector(self.didSelectDoneButton(sender:)))
    }
    
    @objc func didSelectDoneButton(sender: UIButton) {
        self.view.endEditing(true)
        
        if ClienteData.shared.nome.isEmpty ||
            ClienteData.shared.email.isEmpty ||
            ClienteData.shared.empresa.isEmpty ||
            ClienteData.shared.telefone.isEmpty {
            detailView.isHidden = false
            return
        }
        
        ClienteData.shared.createNew {
            ClientesData.shared.shouldUpdate()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case nomeField:
            ClienteData.shared.nome = textField.text ?? ""
            
        case emailField:
            ClienteData.shared.email = textField.text ?? ""
            
        case empresaField:
            ClienteData.shared.empresa = textField.text ?? ""
            
        case telefoneField:
            ClienteData.shared.telefone = textField.text ?? ""
            
        default:
            break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        detailView.isHidden = true
    }
    
    deinit {
        ClienteData.shared.eraseData()
    }

}
