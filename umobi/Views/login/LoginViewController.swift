//
//  LoginViewController.swift
//  umoby-test
//
//  Created by Brenno de Moura on 13/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UINavigationBarDelegate, UITextFieldDelegate {

    @IBOutlet weak var usuarioText: UITextField!
    @IBOutlet weak var senhaText: UITextField!
    @IBOutlet weak var navBar: UINavigationBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navBar.delegate = self
        
        textFieldConfig()
    }
    
    func textFieldConfig() {
        usuarioText.delegate = self
        senhaText.delegate = self
        
        self.textFieldNormalState(usuarioText)
        self.textFieldNormalState(senhaText)
    }
    
    func textFieldNormalState(_ textField: UITextField) {
        textField.layer.borderWidth = 0.1
        textField.layer.cornerRadius = 5
        textField.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func textFieldDangerState(_ textField: UITextField) {
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 5
        textField.layer.borderColor = UIColor.red.cgColor
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case usuarioText:
            UsuarioData.shared.email = textField.text ?? ""
            
        case senhaText:
            UsuarioData.shared.senha = textField.text ?? ""
        
        default:
            break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case usuarioText:
            self.textFieldNormalState(usuarioText)
            
        case senhaText:
            self.textFieldNormalState(senhaText)
            
        default:
            break
        }
    }
    
    @IBAction func didSelectEntrar(_ sender: UIButton) {
        self.view.endEditing(true)
        
        var verified = true
        if !self.checkEmail() {
            self.textFieldDangerState(usuarioText)
            verified = verified && false
        }
        
        if !self.checkSenha() {
            self.textFieldDangerState(senhaText)
            verified = verified && false
        }
        
        if !verified {
            return
        }
        
        UsuarioData.login {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func checkEmail() -> Bool {
        if UsuarioData.shared.email.count >= 1 {
            return true
        }
        
        guard let email = usuarioText.text, email.count >= 1 else {
            return false
        }
        
        UsuarioData.shared.email = email
        return true
    }
    
    func checkSenha() -> Bool {
        if UsuarioData.shared.senha.count >= 1 {
            return true
        }
        
        guard let senha = senhaText.text, senha.count >= 1 else {
            return false
        }
        
        UsuarioData.shared.senha = senha
        return true
    }
    
    @IBAction func didSelectCancelar(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}
