//
//  ViewController.swift
//  umoby-test
//
//  Created by Brenno de Moura on 13/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import UIKit

class ClientesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
        
        setupNavigationBar()
        
        self.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        rightButton.setTitle("Adicionar", for: .normal)
        
        if UsuarioData.shared.id != nil {
            leftButton.setTitle("Sair", for: .normal)
            rightButton.isHidden = false
        } else {
            self.userIsNotLogged()
        }
        
        if !ClientesData.shared.isUpdated {
            self.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailClienteSegue", let detailController = segue.destination as? DetailClienteViewController {
            detailController.id = sender as! Int
        }
    }
    
    func setupNavigationBar() {
        let searchController = UISearchController(searchResultsController: nil)
        self.definesPresentationContext = true
        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Buscar"
        navigationItem.hidesSearchBarWhenScrolling = false
        self.searchBar = searchController.searchBar
    }
    
    func reloadData() {
        ClientesData.request {
            ClientesList.populate(filter: self.searchBar.text)
            self.tableView.reloadData()
        }
    }
    
    func userIsNotLogged() {
        leftButton.setTitle("Entrar", for: .normal)
        rightButton.isHidden = true
    }
    
    @IBAction func didSelectLoginButton(_ sender: UIButton) {
        if UsuarioData.shared.id != nil {
            UsuarioData.shared.eraseData()
            self.userIsNotLogged()
            return
        }
        
        let login = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
        self.present(login, animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ClientesList.array.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "clientesCell", for: indexPath) as! ClientesTableViewCell
        
        DispatchQueue.main.async {
            let cliente = ClientesList.array[indexPath.row]
            cell.label.text = cliente.nome
            cell.tag = cliente.id
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = ClientesList.array[indexPath.row].id
        self.performSegue(withIdentifier: "detailClienteSegue", sender: id)
    }
}

