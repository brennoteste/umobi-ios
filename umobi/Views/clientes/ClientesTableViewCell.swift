//
//  ClientesTableViewCell.swift
//  umoby-test
//
//  Created by Brenno de Moura on 13/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import UIKit

class ClientesTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
