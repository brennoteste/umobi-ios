//
//  ServerRequest.swift
//  umoby-test
//
//  Created by Brenno de Moura on 16/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import Foundation

class Server {
    private static let _url = "http://192.168.1.123/api/"
    
    public static func url(_ path: String) -> URL? {
        return URL(string: self._url + path)
    }
    
    public static func auth(path: String) -> URLRequest? {
        var session = URLRequest(url: Server.url(path)!)
        let loginString = "\(UsuarioData.shared.email):\(UsuarioData.shared.senha)"
        
        guard let loginData = loginString.data(using: .utf8) else {
            return nil
        }
        
        session.setValue("Basic \(loginData.base64EncodedString())", forHTTPHeaderField: "Authorization")
        return session
    }
}
