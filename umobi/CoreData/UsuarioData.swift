//
//  Usuario.swift
//  umoby-test
//
//  Created by Brenno de Moura on 14/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import Foundation
import CoreData

class UsuarioData: DataHandler {
    private static let entity = "Usuario"
    
    private struct Static {
        static var usuario: UsuarioData!
    }
    
    private class var reference: UsuarioData! {
        get {
            if Static.usuario == nil {
                Static.usuario = UsuarioData()
            }
            
            return Static.usuario
        }
    }
    
    static var shared: UsuarioData {
        return self.reference
    }
    
    // MARK: - Data Usuario
    
    private var usuario: Usuario {        
        if let usuario = self.fetchUsuario() {
            return usuario
        }
        
        return self.createObject(forEntityName: UsuarioData.entity) as! Usuario
    }
    
    private func fetchUsuario() -> Usuario?  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: UsuarioData.entity)
        
        if var result = super.fetch(object: request) as? [Usuario], result.count > 0 {
            let usuario = result.removeFirst()
            for usuario in result {
                super.delete(object: usuario)
            }
            
            super.saveContext()
            return usuario
        }
        
        return nil
    }
    
    var email: String {
        get {
            return self.usuario.email ?? ""
        }
        
        set (email) {
            self.usuario.email = email
            super.saveContext()
        }
    }
    
    var senha: String {
        get {
            return self.usuario.senha ?? ""
        }
        
        set (senha) {
            self.usuario.senha = senha
            super.saveContext()
        }
    }
    
    var id: Int? {
        let id = self.usuario.id
        return id == 0 ? nil : Int(id)
    }
    
    public static func login(_ function: @escaping () -> ()) {
        guard let session = Server.auth(path: "usuario") else {
            return
        }
        
        DispatchQueue.main.async {
            URLSession.shared.dataTask(with: session) {
                (data, response, err) in
                
                guard let response = response as? HTTPURLResponse else {
                    return
                }
                
                if response.statusCode != 200 {
                    return
                }
                
                guard let data = data, let model = try? JSONDecoder().decode(UsuarioData.JSONModel.UsuarioAuth.self, from: data) else {
                    return
                }
                
                DispatchQueue.main.async {
                    UsuarioData.shared.usuario.id = Int16(model.usuario_id)
                    UsuarioData.shared.saveContext()
                    function()
                }
                }.resume()
        }
    }
    
    private struct JSONModel {
        public struct UsuarioAuth: Decodable {
            let usuario_id: Int
        }
    }
    
    public func eraseData() {
        super.delete(object: self.usuario)
    }
}
