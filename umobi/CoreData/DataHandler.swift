//
//  DataHandler.swift
//  umoby-test
//
//  Created by Brenno de Moura on 14/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataHandler: NSObject {
    internal weak var controller: DataController! {
        return (UIApplication.shared.delegate as! AppDelegate).dataController
    }
    
    internal weak var sharedContext: NSManagedObjectContext! {
        return self.controller.managedObjectContext
    }
    
    public func saveContext() {
        self.controller.saveContext()
    }
    
    public func createObject(forEntityName: String) -> NSManagedObject {
        return NSEntityDescription.insertNewObject(forEntityName: forEntityName, into: self.sharedContext)
    }
    
    public func fetch(object request: NSFetchRequest<NSFetchRequestResult>) -> [Any]? {
        return try? self.sharedContext.fetch(request)
    }
    
    public func delete(object: NSManagedObject) {
        self.sharedContext.delete(object)
        self.controller.saveContext()
    }
}
