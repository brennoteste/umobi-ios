//
//  ClientesData.swift
//  umoby-test
//
//  Created by Brenno de Moura on 14/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import Foundation
import CoreData

class ClientesData: DataHandler {
    private static let entity = "Clientes"
    
    private struct Static {
        static var clientes: ClientesData!
    }
    
    private class var reference: ClientesData! {
        get {
            if Static.clientes == nil {
                Static.clientes = ClientesData()
            }
            
            return Static.clientes
        }
    }
    
    static var shared: ClientesData {
        return self.reference
    }
    
    
    // MARK: - Data Cliente
    
    private var needsUpdate = false
    
    private var clientes: [Clientes] {
        if let clientes = self.fetchClientes() {
            return clientes
        }
        
        return []
    }
    
    private func fetchClientes(withString: String? = nil) -> [Clientes]?  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: ClientesData.entity)
        if let filter = withString {
            request.predicate = NSPredicate(format: "nome BEGINSWITH[cd] %@", filter)
        }
        
        request.sortDescriptors = [NSSortDescriptor(key: #keyPath(Clientes.nome), ascending: true)]
        
        if let clientes = super.fetch(object: request) as? [Clientes] {
            print(clientes.count)
            return clientes
        }
        
        return nil
    }
    
    public func all() -> [Clientes] {
        return self.clientes
    }
    
    private func cliente() -> Clientes {
        return super.createObject(forEntityName: ClientesData.entity) as! Clientes
    }
    
    var count: Int {
        return self.clientes.count
    }
    
    public static func request(_ function: @escaping () -> ()) {
        let session = URLRequest(url: Server.url("clientes")!)
        DispatchQueue.main.async {
            URLSession.shared.dataTask(with: session) {
            (data, response, err) in
            
                guard let response = response as? HTTPURLResponse else {
                    return
                }
                
                if response.statusCode != 200 {
                    return
                }
            
                guard let data = data, let model = try? JSONDecoder().decode(ClientesData.JSONModel.Clientes.self, from: data) else {
                    return
                }
            
                DispatchQueue.main.async {
                    ClientesData.shared.eraseData()
                    ClientesData.shared.needsUpdate = false
                    
                    for cliente in model.clientes {
                        let handler = ClientesData.shared.cliente()
                        handler.nome = cliente.nome
                        handler.id = Int16(cliente.id)
                    }
                    ClientesData.shared.saveContext()
                    function()
                }
            }.resume()
        }
    }
    
    public func eraseData() {
        for cliente in self.clientes {
            super.delete(object: cliente)
        }
    }
    
    public func shouldUpdate() {
        self.needsUpdate = true
    }
    
    public var isUpdated: Bool {
        return !self.needsUpdate
    }
    
    public func filter(_ string: String) -> [Clientes] {
        return self.fetchClientes(withString: string) ?? []
    }
    
    private struct JSONModel {
        struct Cliente: Decodable {
            let nome: String
            let id: Int
        }
        
        struct Clientes: Decodable {
            let clientes: [Cliente]
        }
    }
}

class ClientesList {
    static var array: [ClientesList] = []
    
    let id: Int
    let nome: String
    
    init(id: Int, nome: String) {
        self.id = id
        self.nome = nome
    }
    
    public static func store(_ cliente: Clientes) {
        let clienteList = ClientesList(id: Int(cliente.id), nome: cliente.nome!)
        ClientesList.array.append(clienteList)
    }
    
    public static func erase() {
        self.array = []
    }
    
    public static func populate(filter: String? = nil) {
        self.erase()
        if let filter = filter, filter.count >= 1 {
            let filtered = ClientesData.shared.filter(filter)
            for cliente in filtered {
                self.store(cliente)
            }
            return
        }
        
        let clientes = ClientesData.shared.all()
        for cliente in clientes {
            self.store(cliente)
        }
    }
}
