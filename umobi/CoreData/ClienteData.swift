//
//  Clientes.swift
//  umoby-test
//
//  Created by Brenno de Moura on 14/02/19.
//  Copyright © 2019 Brenno de Moura. All rights reserved.
//

import Foundation
import CoreData

class ClienteData: DataHandler {
    private static let entity = "Cliente"
    
    private struct Static {
        static var cliente: ClienteData!
    }
    
    private class var reference: ClienteData! {
        get {
            if Static.cliente == nil {
                Static.cliente = ClienteData()
            }
            
            return Static.cliente
        }
    }
    
    static var shared: ClienteData {
        return self.reference
    }
    
    // MARK: - Data Cliente
    
    private var cliente: Cliente {
        if let cliente = self.fetchCliente() {
            return cliente
        }
        
        return self.createObject(forEntityName: ClienteData.entity) as! Cliente
    }
    
    private func fetchCliente() -> Cliente?  {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: ClienteData.entity)
        
        if var result = super.fetch(object: request) as? [Cliente], result.count > 0 {
            let cliente = result.removeFirst()
            for cliente in result {
                super.delete(object: cliente)
            }
            
            super.saveContext()
            return cliente
        }
        
        return nil
    }
    
    var email: String {
        get {
            return self.cliente.email ?? ""
        }
        
        set (email) {
            self.cliente.email = email
            super.saveContext()
        }
    }
    
    var empresa: String {
        get {
            return self.cliente.empresa ?? ""
        }
        
        set (empresa) {
            self.cliente.empresa = empresa
            super.saveContext()
        }
    }
    
    var nome: String {
        get {
            return self.cliente.nome ?? ""
        }
        
        set (nome) {
            self.cliente.nome = nome
            super.saveContext()
        }
    }
    
    var telefone: String {
        get {
            return self.cliente.telefone ?? ""
        }
        
        set (telefone) {
            self.cliente.telefone = telefone
            super.saveContext()
        }
    }
    
    public func eraseData() {
        super.delete(object: self.cliente)
    }
    
    public static func request(id: Int,_ function: @escaping () -> ()) {
        // TO-DO: Servidor request
        let session = URLRequest(url: Server.url("clientes/\(id)")!)
        DispatchQueue.main.async {
            URLSession.shared.dataTask(with: session) {
                (data, response, err) in
                
                guard let response = response as? HTTPURLResponse else {
                    return
                }
                
                if response.statusCode != 200 {
                    return
                }
                
                guard let data = data, let model = try? JSONDecoder().decode(ClienteData.JSONModel.Cliente.self, from: data) else {
                    return
                }
                
                DispatchQueue.main.async {
                    let cliente = ClienteData.shared.cliente
                    cliente.nome = model.nome
                    cliente.email = model.email
                    cliente.telefone = model.telefone
                    cliente.empresa = model.empresa
                    
                    ClienteData.shared.saveContext()
                    function()
                }
            }.resume()
        }
    }
    
    public func createNew(_ function: @escaping () -> ()) {
        guard var session = Server.auth(path: "clientes") else {
            return
        }
        
        session.httpMethod = "POST"
        session.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        session.httpBody = "nome=\(self.cliente.nome ?? "")&email=\(self.cliente.email ?? "")&telefone=\(self.cliente.telefone ?? "")&empresa=\(self.cliente.empresa ?? "")".data(using: .utf8)
        
        DispatchQueue.main.async {
            URLSession.shared.dataTask(with: session) {
                (data, response, err) in
                
                guard let response = response as? HTTPURLResponse else {
                    return
                }
                
                if response.statusCode != 200 {
                    return
                }
                
                DispatchQueue.main.async {
                    function()
                }
                }.resume()
        }
        //function()
    }
    
    private struct JSONModel {
        public struct Cliente: Decodable {
            let nome: String
            let empresa: String
            let telefone: String
            let email: String
        }
    }
}
